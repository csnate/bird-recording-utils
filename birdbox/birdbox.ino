// Bird Box Sketch
// Logs RFID reads

#include <stdint.h>
#include <stdlib.h>

#include <SD.h>
#include <SoftwareSerial.h>
#include <Wire.h>
#include "RTClib.h"

#define MIN_CMD_LENGTH 4
#define CMD_GET_TIME 0
#define CMD_SET_TIME 1

#define SD_CHIPSELECT 10
#define SD_PIN 10
#define SD_LOG_FILE "rfidlog.csv"

RTC_DS1307 RTC;
SoftwareSerial rfidSerial(4, 5);

void setup()
{
  Serial.begin(9600);
  rfidSerial.begin(9600);
  Wire.begin();
  RTC.begin();
  
  /// If the RTC is not yet running, set its clock to epoch. (Jan 1, 1970)
  if(!RTC.isrunning())
    RTC.adjust(DateTime(0));
    
  pinMode(10, OUTPUT);
  
  if(!SD.begin(SD_CHIPSELECT))
    return;
}

/// Writes the current Unix time to the serial port.
void doGetTime()
{
  uint32_t ctime = RTC.now().unixtime();
  uint8_t* ptr = (uint8_t*) &ctime;
  for(uint8_t i = 0; i < 4; ++i)
    Serial.write(ptr[i]);
}

/// Read the current unix time from the serial port.  The payload
/// in the command is expected to be of size 'len' bytes.
void doSetTime(uint8_t len)
{
  delay(50); // Need to delay so that we actually have some data.
  
  // Read time from the serial port.
  uint8_t* payload = (uint8_t*) malloc(len * sizeof(*payload));
  for(uint8_t i = 0; i < len; ++i)
    payload[i] = Serial.read();
  
  // convert the byte array to a 32-bit unsigned integer that
  // represents unix time.
  uint32_t ntime = (uint32_t) payload[0]        |
                   (uint32_t) payload[1] << 8   |
                   (uint32_t) payload[2] << 16  |
                   (uint32_t) payload[3] << 24;
  
  // set the real-time clock.
  RTC.adjust(DateTime(ntime));
  free(payload);
}

/// Process a command from the serial port.
void processCommand()
{
  while(Serial.available() >= MIN_CMD_LENGTH) {
    
    // Attempt to read the command header.
    uint8_t header[2];
    header[0] = Serial.read();
    header[1] = Serial.read();
    
    // If the header is found, process the command.
    if(header[0] == 0xFF && header[1] == 0xBB) {
      uint8_t cmd = Serial.read();
      uint8_t payload_length = Serial.read();
      
      if(cmd == CMD_GET_TIME) // Requested the time.
        doGetTime();
      else if(cmd == CMD_SET_TIME) // Setting the time.
        doSetTime(payload_length);
    }
    
  }
}

void readRfid(uint8_t* code)
{
  byte i = 0;
  byte val = 0;
  byte checksum = 0;
  byte bytesread = 0;
  byte tempbyte = 0;

  if(rfidSerial.available() > 0) {
    if((val = rfidSerial.read()) == 2) {                  // check for header 
      bytesread = 0; 
      while (bytesread < 12) {                        // read 10 digit code + 2 digit checksum
        if( rfidSerial.available() > 0) { 
          val = rfidSerial.read();
          if((val == 0x0D)||(val == 0x0A)||(val == 0x03)||(val == 0x02)) { // if header or stop bytes before the 10 digit reading 
            break;                                    // stop reading
          }

          // Do Ascii/Hex conversion:
          if ((val >= '0') && (val <= '9')) {
            val = val - '0';
          } else if ((val >= 'A') && (val <= 'F')) {
            val = 10 + val - 'A';
          }

          // Every two hex-digits, add byte to code:
          if (bytesread & 1 == 1) {
            // make some space for this hex-digit by
            // shifting the previous hex-digit with 4 bits to the left:
            code[bytesread >> 1] = (val | (tempbyte << 4));

            if (bytesread >> 1 != 5) {                // If we're at the checksum byte,
              checksum ^= code[bytesread >> 1];       // Calculate the checksum... (XOR)
            };
          } else {
            tempbyte = val;                           // Store the first hex digit first...
          };

          bytesread++;                                // ready to read next digit
        } 
      }
    }
  }
}

// Writes a comma separated entry to the file.
void writeEntry(uint32_t timestamp, uint8_t* code)
{
  File datalog = SD.open(SD_LOG_FILE, FILE_WRITE);
  
  if(datalog) {
    String entry = "";
    entry += timestamp;
    entry += ',';
    for(byte i = 0; i < 5; ++i) {
      if(code[i] < 6) entry += '0';
      entry += String(code[i], HEX);
    }
    datalog.println(entry);
    datalog.close();
  }
}

void loop()
{
  // Check for commands on the serial port.  If a command is processed,
  // return so that no RFID reads are logged until the next loop iteration.
  if(Serial.available() > 0) {
    processCommand();
    return;
  }
  
  // Looks like there is an RIFD read available, read it and log to file.
  if(rfidSerial.available() > 0) {
    uint8_t code[6];
    readRfid(code);
    uint32_t timestamp = RTC.now().unixtime();
    writeEntry(timestamp, code);
    
    rfidSerial.flush();
  }
}
